# Copyright (c) 2018-2020. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl

param(
  [Parameter(Mandatory=$True,HelpMessage="Path to the file to extract")]
  [string]$file,
  [Parameter(Mandatory=$True,HelpMessage="Path to the folder to extract into")]
  [string]$dir
)

Write-Host "  Extracting $file..."
Try {
  Expand-Archive $file -DestinationPath $dir
  $host.SetShouldExit(0)
  Break
} Catch {
  $host.SetShouldExit(1)
  Break
}

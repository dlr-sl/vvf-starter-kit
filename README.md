# VVF Starter Kit

The [*VVF Starter Kit*](https://gitlab.com/dlr-sl/vvf-starter-kit) helps maritime engineers and researchers to set up the *Virtual Vessel Framework* on their (Windows) computers, developed by DLR for the project `HOLISHIP`, funded by the European Commission under grant number *689074*.

The VVF starter kit installs a software environment consisting of
- [RCE platform](http://www.rcenvironment.de) - a versatile collaboration, software integration and orchestration platform
- [HOLISPEC documents](https://www.gitlab.com/DLR-SL/holiSpec) - a maritime technical language for data exchange and parametric model description (work in progress)
- Additional libraries, examples and service scripts (work in progress)
- Integrated engineering tools (pending)

The VVF Starter Kit itself consists of a few service scripts needed to install the VVF into a single, readily bundled and easily accessible platform for experimentation, integration and operation.


## Requirements
- Windows 7 or higher
- Powershell 5 or higher
- 64-bit processor architecture


## Getting started
- Download and extract the bundled VVF starter kit distribution `vvf.zip` [from the HOLISPEC platform](http://www.holiship.eu/downloads/)
- Unzip the contents (of the archived `vvf` folder) into a location where you have access rights to create, modify and write files, ideally using a short path prefix e.g. `D:\vvf`
- If you intend to reuse an existing Python `conda` distribution on your system, make sure that your user account has access rights to manage the defined virtual environments
- Open a command shell (`cmd.exe`) in the unzipped VVF folder and run `vvf run`
- This will download and install all tools needed to run VVF, and set up a full development and integration environment
- In case of problems, check if your `PATH` environment variable is not tool long and doesn't contain invalid entries.


## Further information
To avoid licensing issues, the VVF starter kit will download any missing dependencies when first run, and place them into a subfolder.
No other part of the file system will be touched, VVF can be reset by running `vvf clean` and any changes can easily be undone by removing the entire VVF folder.

To get started, download and extract the bundled VVF distribution [from the HOLISPEC platform](http://www.holiship.eu/downloads/); when starting from scratch using only the Git checkout, a additional files must be put into the checkout (e.g. graphics for product branding and a minimal set of software libraries); refer to `build.bat` to see the minimum set of files required.


## Technical details
Here is a schematic and slightly simplified flow chart of the VVF bootstrap script.

![VVF bootstrap flow chart](./bootstrap.png)

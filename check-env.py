# coding=utf-8

''' Copyright (c) 2018-2020. Deutsches Zentrum für Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl
    This script checks if we are in a virtual Python environment.
'''

import os, sys
sys.exit(0 if os.path.exists(os.path.join(sys.argv[1], "python.exe")) else 1)

# Copyright (c) 2018-2020. Deutsches Zentrum für Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl
# coding=utf-8

import os, sys

# Error code 2 means "environment not compatible, requirements not sufficiently met"
if " " in os.getcwd():
  print("  VVF path and folder name must not contain spaces. Please rename them before continuing")
  sys.exit(2)

if len(os.getcwd()) > 15:
  print("  Due to very long file paths, please run the VVF starter kit from a location with a very short path, e.g. from 'D:\\VVF' or 'C:\\apps\\vvf'")
  sys.exit(2)

# https://software.intel.com/en-us/articles/limitation-to-the-length-of-the-system-path-variable
if len(os.environ["PATH"]) + 5 * (30 + len(os.path.abspath(os.getcwd()))) > 2047:
  print("  Due many entries on the PATH variable, VVF cannot extend PATH as required. Please shorten the PATH environment variable before continuing")
  sys.exit(2)

if sys.version_info.major < 3:
  print("  Not running on Python 3 (but on '%d.%d' instead)" % sys.version_info[:2])
  sys.exit(2)

if not ('conda' in sys.version or 'Continuum' in sys.version or os.path.exists(os.path.join(os.path.dirname(sys.executable), "Scripts", "conda.exe"))):
  print("  Not running on Anaconda or Miniconda distribution (but on '%s' instead)" % sys.version)
  sys.exit(2)

if any([path in sys.executable for path in ("ProgramFiles", "ProgramData")]):
  print("  Not running in a user-based Python installation (but in system-wide '%s' instead)" % os.path.dirname(sys.executable))
  #sys.exit(2)

if sys.platform != "win32":
  print("  Not running on a Windows platform (but on '%s' instead)" % sys.platform)
  sys.exit(2)

# Copyright (c) 2018-2020. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl

param(
  [string]$urlp,
  [string]$file
)

# Hack: https://www.frankysweb.de/powershell-es-konnte-kein-geschuetzter-ssltls-kanal-erstellt-werden/
$AllProtocols = [System.Net.SecurityProtocolType]'Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols

Write-Host "  Downloading $file..."
$client = new-object System.Net.WebClient
Try {
  $client.DownloadFile("$urlp", "apps\$file")
  $host.SetShouldExit(0)
  Break
} Catch {
  Try {
    Invoke-WebRequest $urlp -Out apps\$file
    $host.SetShouldExit(0)
    Break
  } catch {
    Write-Host "    Error downloading $file from $urlp..."
    $host.SetShouldExit(1)
  }
}

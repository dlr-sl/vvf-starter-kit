# coding=utf-8

''' Copyright (c) 2018-2020. Deutsches Zentrum für Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl '''

import subprocess, sys

out = subprocess.Popen("java -version", shell = True, bufsize = 1, stdout = subprocess.PIPE, stderr = subprocess.STDOUT).communicate()[0]
if b'version' not in out or b'No Java found' in out:
  print("  No Java found"); sys.exit(1)
if b"Client VM" in out or b"SE Runtime Environment" in out:
  print("  No Java JDK/server VM found (but found a JRE/client VM instead)"); sys.exit(1)

@echo off
rem This script creates a minimal distribution package for use in HOLISHIP
rem TODO provide self-extracting installer via https://www.hofmann-robert.info/computer/2017/09/19/selbstextrahierendes-archiv-mit-7-zip-erstellen.html

del /F /Q vvf.zip >nul 2>&1
mkdir build >nul 2>&1
mkdir build\vvf >nul 2>&1
java -jar %PLANTUML_PATH%\plantuml.jar bootstrap.puml
pandoc --verbose -f markdown+fenced_code_attributes+backtick_code_blocks+inline_code_attributes+smart --pdf-engine=pdflatex -V papersize=A4 -V classoption=a4paper -V fontsize=10pt -V documentclass=article -V margin-left=1in -V margin-right=.7in -V margin-top=1in -V margin-bottom=1in -V colorlinks -o "build\vvf\README.pdf" "README.md" > nul
copy autocook.tar.gz build\vvf\ >nul 2>&1
copy rceusria.tar.gz build\vvf\ >nul 2>&1
copy splash.bmp build\vvf\ >nul 2>&1
copy about.png build\vvf\ >nul 2>&1
copy editor-background.png build\vvf\ >nul 2>&1
copy vvf-logo.ico build\vvf\ >nul 2>&1
copy LICENSE build\vvf\ >nul 2>&1
copy RceUserInteraction.* build\vvf\ >nul 2>&1
copy "*.bat" build\vvf\ >nul 2>&1
del /Q /F build\vvf\build.bat >nul 2>&1
copy "*.md" build\vvf\ >nul 2>&1
copy "*.py" build\vvf\ >nul 2>&1
copy "*.ps1" build\vvf\ >nul 2>&1
copy "*.yaml" build\vvf\ >nul 2>&1
copy "*.pdf" build\vvf\ >nul 2>&1
set PATH=%CD%\apps;%PATH%
cd build
..\7z a -tzip ..\vvf.zip -bb3 -mx9 -mmt2 -sccWIN vvf
cd ..
rmdir /S /Q build\ >nul 2>&1

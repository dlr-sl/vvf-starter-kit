@echo off
@rem Copyright (c) 2018-2020. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl

if not exist "%CD%\.git\HEAD" (
  echo Synchronizing with Gitlab repository...
    git init
    git remote add origin https://gitlab.com/dlr-sl/vvf-starter-kit.git
    git fetch
    git checkout origin/master -ft
    if errorlevel 1 (
      echo   Failed to synchronize the VVF starter kit from Gitlab, please correct manually.
    ) else (
      echo   VVF Starter Kit was successfully synchronized.
    )
) else (
  echo Upgrading VVF starter kit from Gitlab repository...
  git stash
  git pull --rebase
  git checkout master
  if errorlevel 1 (
    echo   Failed to upgrade the VVF starter kit from Gitlab, please correct manually.
  ) else (
    echo   VVF Starter Kit was successfully upgraded.
  )
)
echo .
echo .
echo Please close this window NOW, do not use it!
rem The following line quits AND removes upgrade2.bat
(goto) 2>nul & del "%~f0"

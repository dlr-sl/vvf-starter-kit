@echo off
rem Copyright (c) 2018-2020. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl

mkdir apps >nul 2>&1
echo Checking Powershell...
powershell -? >nul 2>&1
if errorlevel 1 (
  set "PATH=C:\Windows\System32\WindowsPowerShell\v1.0;C:\Windows\System32;%PATH%"
  powershell -? >nul 2>&1
  if errorlevel 1 (
    echo   Powershell not found, cannot continue.
    goto :ERROR
  )
)
echo   OK.

echo Checking folder rights...
echo Testing write rights > %CD%\_tmp_.txt >nul 2>&1
if errorlevel 1 (
  echo   Cannot write inside VVF folder. Please check folder access rights on %CD% before continuing
  goto :ERROR
)
del /F /Q %CD%\_tmp_.txt >nul 2>&1
echo   OK.

echo Checking console rights...
powershell .\policy.ps1
if errorlevel 1 (
  echo   Insufficient rights to run Powershell scripts. Please run the following command in an elevated Powershell session:
  echo     Set-Executionpolicy RemoteSigned -Force
  goto :ERROR
)
if errorlevel 2 (
  echo   Wrong Powershell version detected. Needs Powershell version 5 or higher to operate.
  goto :ERROR
)
echo   OK.

echo Checking 7Zip...
if not exist "%CD%\apps\7z.exe" (
  powershell .\download.ps1 'https://www.7-zip.org/a/%ZIP7%' '%ZIP7%'
  msiexec /a "%CD%\apps\%ZIP7%" /qn /quiet TARGETDIR="%CD%\apps\7z\"
  move /Y "%CD%\apps\7z\Files\7-Zip\7z.exe" "%CD%\apps\" >nul
  move /Y "%CD%\apps\7z\Files\7-Zip\7z.dll" "%CD%\apps\" >nul
  rmdir /q /s %CD%\apps\7z\ >nul 2>&1
)
echo   OK.

echo Checking Python...
python check-python.py >nul 2>&1
if errorlevel 9009 (
  set error=nopython
) else (
  if errorlevel 2 (
    rem Run once again to print the error output (way suppressed above)
    python check-python.py
    set error=other
  ) else (
    if errorlevel 1 (
      set error=nopython
      echo   Python is missing.
    ) else (
      set error=ok
      echo   OK.
    )
  )
)

if "%error%" == "other" (
  rem The error message was already written out
  goto :ERROR
) else (
  if "%error%" == "nopython" (
    if not exist "%CD%\apps\Miniconda3\python.exe" (
      if not exist "%CD%\apps\%CONDA_EXE%" (
        powershell .\download.ps1 'https://repo.continuum.io/miniconda/%CONDA_EXE%' '%CONDA_EXE%'
        if errorlevel 1 (
          echo   Cannot download Miniconda Python 3 distribution.
          goto :ERROR
        )
      )
      echo   Putting downloaded Miniconda Python 3.x into subfolder...
      mkdir "%CD%\apps\Miniconda3" >nul 2>&1
      rem https://conda.io/docs/user-guide/install/windows.html#install-win-silent
      cmd /C "apps\%CONDA_EXE%" /S /InstallationType=JustMe /AddToPath=0 /RegisterPython=0 /NoRegistry=1 /D=%CD%\apps\Miniconda3
      if errorlevel 1 (
        echo   Could not put local Python installation under ./apps/Miniconda3.
        goto :ERROR
      )
      rem Hacky installation to support unicode on Windows console
      set "OLD_ENCODING=%PYTHONIOENCODING%"
      set "PYTHONIOENCODING=UTF-8"
      call apps\Miniconda3\Scripts\pip install win-unicode-console >nul 2>&1
      set "PYTHONIOENCODING=%OLD_ENCODING%"
    ) else (
      echo   Using previously downloaded Miniconda for Python 3.
    );
  )
)
rem Bugfix for SSL error
copy /Y "%CD%\apps\Miniconda3\Library\bin\libcrypto-1_1-x64.*" "%CD%\apps\Miniconda3\DLLs"\ >nul 2>&1
copy /Y "%CD%\apps\Miniconda3\Library\bin\libssl-1_1-x64.*"    "%CD%\apps\Miniconda3\DLLs"\ >nul 2>&1

rem We need the global conda command before setting up the environment
set "PATH=%CD%\apps\Miniconda3\Scripts;apps\Miniconda3;%CD%\Miniconda3\Library\bin;%PATH%"
rem ;%CD%\apps\Miniconda3\Library\mingw-w64\bin;%CD%\apps\Miniconda3\Library\usr\bin;%CD%\apps\Miniconda3\bin;%CD%\apps\Miniconda3\condabin;%PATH%"
rem https://docs.conda.io/projects/conda/en/latest/user-guide/troubleshooting.html?highlight=ssl#ssl-connection-errors

echo Checking Git...
git version >nul 2>&1
if errorlevel 1 (
  echo   Global Git installation not found
  if not exist "%CD%\apps\Git\bin\git.exe" (
    echo   Git in subfolder not found
    if not exist "%CD%\apps\%GIT_ZIP%" (
      powershell .\download.ps1 'https://github.com/git-for-windows/git/releases/download/v%GIT_VERSION%.windows.1/%GIT_ZIP%' '%GIT_ZIP%'
      if errorlevel 1 (
        echo Error downloading Portable Git, continuing.
      )
    )
    echo   Extracting Git...
    apps\7z x -oapps\Git apps\%GIT_ZIP%
  )
  echo   Using Git from subfolder
  set "PATH=%CD%\apps\Git\bin;%PATH%"
)
echo   OK.

echo Checking Java...
if not exist "%CD%\apps\jdk8\LICENSE" (
  rem Java already installed and available
  python check-java.py
  if errorlevel 1 (
    echo Putting Java 8 JDK into subfolder...
    if not exist "%CD%\apps\%JAVA_ZIP%" (
      powershell .\download.ps1 'https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u212-b03/%JAVA_ZIP%' '%JAVA_ZIP%'
    )
    powershell .\extract.ps1 'apps\%JAVA_ZIP%' apps
    move apps\jdk8u212-b03 apps\jdk8 >nul 2>&1
rem This code is for the Oracle JDK 8
rem From http://jdk.java.net/java-se-ri/8 via http://jdk.java.net/
rem    mkdir apps\jdk8 >nul 2>&1
rem    cd apps\jdk8
rem    ..\7z e ..\jdk-8u171-windows-x64.exe .rsrc/1033/JAVA_CAB10/111 >nul 2>&1
rem    ..\7z e 111 >nul 2>&1
rem    del 111
rem    ..\7z x -o. tools.zip >nul 2>&1
rem    del tools.zip
rem    bin\unpack200 lib\tools.pack lib\tools.jar
rem    bin\unpack200 jre\lib\ext\localedata.pack jre\lib\ext\localedata.jar
rem    bin\unpack200 jre\lib\charsets.pack jre\lib\charsets.jar
rem    bin\unpack200 jre\lib\jsse.pack jre\lib\jsse.jar
rem    bin\unpack200 jre\lib\rt.pack jre\lib\rt.jar
rem    bin\unpack200 jre\lib\deploy.pack jre\lib\deploy.jar
rem    bin\unpack200 jre\lib\plugin.pack jre\lib\plugin.jar
rem    cd ..\..
  )
)
set "JAVA_HOME=%CD%\apps\jdk8"
set "PATH=%JAVA_HOME%\bin;%PATH%"
echo   OK.
echo Checking RCE...
if not exist "%CD%\apps\rce\rce.exe" (
  if not exist "%CD%\apps\%RCE_ZIP%" (
    powershell .\download.ps1 '%RCE_PATH%/%RCE_ZIP%' '%RCE_ZIP%'
    if errorlevel 1 (
      echo   Error downloading RCE; exiting.
      goto :ERROR
    )
  )
  powershell .\extract.ps1 'apps\%RCE_ZIP%' apps
  if errorlevel 1 (
    echo   Error extracting RCE. Please unzip the contents of .\apps\rce-*.zip into the .\apps\ folder manually and restart vvf; exiting.
    goto :ERROR
  )
)
echo   OK.
exit /b 0
goto :EOF

:ERROR
exit /b 1

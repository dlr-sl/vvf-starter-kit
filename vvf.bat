rem Copyright (c) 2018-2020. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl
@echo off
pushd
cd %~dp0
if exist "%CD%\vvf.lock" (
  echo #
  echo VVF is already running or was not cleanly shut down.
  echo Please check running processes for VVF, or remove lock file "vvf.lock" if VVF is not running.
  echo #
  echo Press space to exit.
  pause
  popd
  exit /b 1
  goto :EOF
)
echo VVF is running! > "%CD%\vvf.lock"

rem Constants
set VER=0.7.0
set ENV=vvf-%VER%
set "VVFENV=%CD%\apps\Miniconda3\envs\%ENV%"
set "OLD_PATH=%PATH%"
set "OLD_JAVA_HOME=%JAVA_HOME%"
set ZIP7=7z1900-x64.msi
set CONDA_EXE=Miniconda3-py38_4.8.3-Windows-x86_64.exe
set GIT_VERSION=2.26.2
set GIT_ZIP=PortableGit-%GIT_VERSION%-64-bit.7z.exe
set JAVA_ZIP=OpenJDK8U-jdk_x64_windows_hotspot_8u212b03.zip
set RCE_ZIP=rce-10.2.0.202011111224-standard-win32.x86_64.zip
set RCE_PATH=https://updates-external.sc.dlr.de/rce/10.x/products/standard/releases/10.2.0/zip
set BRAND_SP=de.rcenvironment.core.gui.branding.default_10.2.0.202011111224
set BRAND_WF=de.rcenvironment.core.gui.workflow.branding.default_10.2.0.202011111224


rem ##### Command line parsing
echo VVF installer V%VER%
set help=false
if "%1" == "help" (
  set help=true
)
if "%1" == "--help" (
  set help=true
)
if "%1" == "-h" (
  set help=true
)
if "%1" == "/?" (
  set help=true
)
if "%1" == "" (
  set help=true
)

if "%help%" == "true" (
  rem Quick exit
  echo Usage:  vvf [install] [update [libs ^| rce ^| vvf]] [run] [clean [all]]
  echo Usage:  vvf [help]
  echo   install:     install VVF with RCE, Git, Java, Python and into a local subfolder ^(not global system modification^)
  echo   update:      update essential Python tools ^(pip, conda^)
  echo     libs:         update all installed Python libraries ^(use with caution^)
  echo     rce:         update installed RCE version ^(creates backup of previous one^)
  echo     vvf          update VVF framework scripts from online repository, then run 'update rce'
  echo   run:         start VVF GUI ^(RCE^)
  echo   clean:       remove Python environment
  echo     all:         remove all installed tools ^(Git, Java, Python^)
  echo   help:        this information
  goto :END
)

set install=false
if "%1" == "install" (
  set install=true
)

set update=false
if "%1" == "update" (
  set update=true
  if "%2" == "libs" (
    set update=libs
  )
  if "%2" == "rce" (
    set update=rce
  )
  if "%2" == "vvf" (
    set update=vvf
  )
)
if "%2" == "update" (
  set update=true
  if "%3" == "libs" (
    set update=libs
  )
  if "%3" == "rce" (
    set update=rce
  )
  if "%3" == "vvf" (
    set update=vvf
  )
)

set run=false
if "%1" == "run" (
  set run=true
)
if "%2" == "run" (
  set run=true
)
if "%3" == "run" (
  set run=true
)
if "%4" == "run" (
  set run=true
)

set clean=false
if "%1" == "clean" (
  set clean=true
  if "%2" == "all" (
    set clean=all
  )
)
if "%2" == "clean" (
  set clean=true
  if "%3" == "all" (
    set clean=all
  )
)
if "%3" == "clean" (
  set clean=true
  if "%4" == "all" (
    set clean=all
  )
)
if "%4" == "clean" (
  set clean=true
  if "%5" == "all" (
    set clean=all
  )
)
if "%5" == "clean" (
  set clean=true
  if "%6" == "all" (
    set clean=all
  )
)

rem ##### Main script processing
if "%clean%" == "false" (
  call setup-deps.bat
)
if errorlevel 1 (
  rem Something went wrong during setup
rem  set "PATH=%OLD_PATH%"
rem  set "JAVA_HOME=%OLD_JAVA_HOME%"
rem  set "OLD_PATH="
rem  set "OLD_JAVA_HOME="
  goto :ERROR
)

rem Checks if the Python environment was already installed
rem TODO if exists, will not run autocook!
if "%run%" == "true" (
  python check-env.py %VVFENV%
  if errorlevel 1 (
    set install=true
  ) else (
    set install=false
  )
)


rem ##### Main execution logic TODO download and installation should be separate from environment creation!
if "%install%" == "true" (
  mkdir "%CD%\apps\rcetemp" >nul 2>&1
  echo Setting up VVF conda environment...
  powershell .\download.ps1 https://conda.anaconda.org:443/conda-forge/noarch/repodata.json.bz2 Conda_Repository_Test.delete_me >nul 2>&1
  if errorlevel 1 (
    echo   Cannot access the conda packages repository, maybe no internet connection, exiting.
    goto :ERROR
  ) else (
    del /F /Q apps\Conda_Repository_Test.delete_me
  )
  mkdir "%CD%\apps\Miniconda3" >nul 2>&1
  mkdir "%CD%\apps\Miniconda3\envs" >nul 2>&1
  set "PATH=%VVFENV%\Scripts;%VVFENV%\Lib\site-packages\pywin32_system32;%PATH%"
  call conda create  -p "%VVFENV%" python=3.6 -y --override-channels -c defaults -c dlr-sc -c conda-forge
  call conda install -p "%VVFENV%"            -y --override-channels -c defaults -c dlr-sc -c conda-forge tixi PyCrypto ruamel.yaml
  rem call "%VVFENV%\Scripts\pip" install winshell pywin32
  rem We would need to run this as admin: "%VVFENV%\Scripts\pywin32_postinstall.py" -install
  rem Alternatively we put the original DLL folder to the path
  call "%VVFENV%\Scripts\pip" install autocook.tar.gz rceusria.tar.gz
  call "%VVFENV%\Scripts\autocook" autocook.yaml --no-subprocess
  echo   OK.
  set update=false
) else (
  set "PATH=%VVFENV%\Scripts;%VVFENV%\Lib\site-packages\pywin32_system32;%PATH%"
)

if "%update%" == "true" (
  echo Updating VVF Python tools...
  "%VVFENV%\Scripts\pip" install --upgrade pip
  call conda update conda -y
  echo   OK.
)
if "%update%" == "libs" (
  echo Updating VVF Python libraries...
  "%VVFENV%\Scripts\pip" install --upgrade pip
  call conda update conda -y
  call conda update -p "%VVFENV%" --all -y
  echo   OK.
)
if "%update%" == "rce" (
  rem TODO re-run Autocook necessary?
  echo Updating RCE...
  if not exist "%CD%\apps\%RCE_ZIP%" (
    echo Updating RCE to version %RCE_ZIP%...
    rmdir /Q /S "%CD%\apps\rce_prev" >nul 2>&1
    move "%CD%\apps\rce" "%CD%\apps\rce_prev" >nul 2>&1
    powershell .\download.ps1 '%RCE_PATH%/%RCE_ZIP%' '%RCE_ZIP%'
    if errorlevel 1 (
      echo   Error downloading RCE; exiting.
      goto :ERROR
    )
    powershell .\extract.ps1 'apps\%RCE_ZIP%' apps
    if errorlevel 1 (
      echo   Error extracting RCE. Please unzip the contents of .\apps\rce-*.zip into the .\apps\ folder manually and restart the VVF; exiting.
      goto :ERROR
    )
    echo   OK.
  ) else (
    echo   Latest RCE version is already installed; exiting.
    goto :END
  )
)
if "%update%" == "vvf" (
  rem Copy the script first to avoid upgrading the batch file while running
  copy upgrade.bat upgrade2.bat
  start "Upgrading VVF" upgrade2.bat
  rem the following jump is executed immediately, while upgrade2.bat is just starting:
  goto :END
)

rem Define variables before "if", because the entire block is evaluated at once (would otherwise replace %profile% with empty string)
set "profile=%CD%\profiles\default"
set "data=%CD%\workspace"
set "rcetemp=%CD%\apps\rcetemp"
rem Using -D below to set Java's temp folder doesn't work, we have to patch TEMP and/or TMP sytem variables
set "oldtemp=%TEMP%"
set "oldtmp=%TMP%""
set "TEMP=%rcetemp%"
set "TMP=%rcetemp%"
if "%run%" == "true" (
  echo Running VVF. Once finished, close RCE to get back to the command line...
  call activate "%VVFENV%"
  pushd apps\rce
  rce --console --profile "%profile%" -data "%data%" "-Djava.io.tmpdir=%rcetemp%"
  popd
  rem This works from the shell, but not when double-clicking the VVF logo:
  call conda deactivate
  echo   OK.
)
set "TEMP=%oldtemp"
set "TMP=%oldtmp"

if "%clean%" NEQ "true" if "%clean%" NEQ "all" (
  rem Above is a special way of coding "OR"
) else (
  echo Removing VVF Python environment...
  call conda remove -p "%VVFENV%" --all -y
  echo   You might want to remove older Python environment as well under "%VVFENV%"
  call "%VVFENV%\Scripts\autocook" uninstall.yaml --no-subprocess
  echo   OK.
)
if "%clean%" == "all" (
  echo Removing all software dependencies...
  rem del /F /Q "apps\%CONDA_EXE%" >nul 2>&1
  rmdir /Q /S apps\Git >nul 2>&1
  rmdir /Q /S apps\jdk8 >nul 2>&1
  rmdir /Q /S apps\Miniconda3 >nul 2>&1
  del /F /Q "%CD%\apps\rce\ws_set_up.dat" >nul 2>&1
  echo   OK.
)

:END
rem set "PATH=%OLD_PATH%"
rem set "JAVA_HOME=%OLD_JAVA_HOME%"
rem set "OLD_PATH="
rem set "OLD_JAVA_HOME="
echo VVF script finished.

del /F /Q "%CD%\vvf.lock"
popd
exit /b 0
goto :EOF

:ERROR
del /F /Q "%CD%\vvf.lock"
popd
exit /b 1

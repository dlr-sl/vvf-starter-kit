# Copyright (c) 2018-2020. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/sl

$version = $PSVersionTable.PSVersion.Major
if ($version -Lt 5) {
  $host.SetShouldExit(2)
  Break
}
